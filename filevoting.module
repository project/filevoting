<?php
/**
 * Main file for the File Voting module.
 */

/**
 * Implements hook_field_formatter_info_alter().
 */
function filevoting_field_formatter_info_alter(&$info) {
  foreach (_filevoting_formatters_list() as $formatter) {
    if (isset($info[$formatter])) {
      $info[$formatter]['settings']['filevoting_enabled'] = FALSE;
      $info[$formatter]['settings']['filevoting'] = array(
        'tag' => 'vote',
        'stars' => 5,
        'allow_clear' => FALSE,
        'allow_revote' => FALSE,
        'allow_ownvote' => FALSE,
        'widget' => array('fivestar_widget' => NULL),
        'style' => 'average',
        'text' => 'average',
      );
    }
  }
}

/**
 * Implements hook_field_formatter_settings_form_alter().
 */
function filevoting_field_formatter_settings_form_alter(&$settings_form, $context) {
  $display = $context['instance']['display'][$context['view_mode']];
  $settings = $display['settings'];

  if (in_array($display['type'], _filevoting_formatters_list())) {
    $settings_form['filevoting_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable voting'),
      '#default_value' => $settings['filevoting_enabled'],
    );

    $settings_form['filevoting'] = array(
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => t('Voting setings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#states' => array(
        'visible' => array(
          ':input[name="fields[' . $context['field']['field_name'] . '][settings_edit_form][settings][filevoting_enabled]"]' => array('checked' => TRUE),
        ),
      ),
    );

    $settings_form['filevoting']['tag'] = array(
      '#type' => 'select',
      '#title' => t('Voting Tag'),
      '#options' => fivestar_get_tags(),
      '#default_value' => $settings['filevoting']['tag'],
    );

    $settings_form['filevoting']['stars'] = array(
      '#type' => 'select',
      '#title' => t('Number of stars'),
      '#options' => drupal_map_assoc(range(1, 10)),
      '#default_value' => $settings['filevoting']['stars'],
    );

    $settings_form['filevoting']['allow_clear'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow users to cancel their ratings.'),
      '#default_value' => $settings['filevoting']['allow_clear'],
      '#return_value' => 1,
    );

    $settings_form['filevoting']['allow_revote'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow users to re-vote on already voted content.'),
      '#default_value' => $settings['filevoting']['allow_revote'],
      '#return_value' => 1,
    );

    $settings_form['filevoting']['allow_ownvote'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow users to vote on their own content.'),
      '#default_value' => $settings['filevoting']['allow_ownvote'],
      '#return_value' => 1,
    );

    $settings_form['filevoting']['widget'] = array(
      '#type' => 'fieldset',
      '#title' => t('Star display options'),
      '#description' => t('Choose a style for your widget.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $widgets = module_invoke_all('fivestar_widgets');
    $settings_form['filevoting']['widget']['fivestar_widget'] = array(
      '#type' => 'radios',
      '#options' => array('default' => t('Default')) + $widgets,
      '#default_value' => isset($settings['filevoting']['widget']['fivestar_widget']) ? $settings['filevoting']['widget']['fivestar_widget'] : 'default',
      '#attributes' => array('class' => array('fivestar-widgets', 'clearfix')),
      '#pre_render' => array('fivestar_previews_expand'),
      '#attached' => array('css' => array(drupal_get_path('module', 'fivestar') . '/css/fivestar-admin.css')),
    );

    $settings_form['filevoting']['style'] = array(
      '#type' => 'select',
      '#title' => t('Value to display as stars'),
      '#default_value' => $settings['filevoting']['style'],
      '#options' => array(
        'average' => t('Average vote'),
        'user'    => t("User's vote"),
        'smart'   => t("User's vote if available, average otherwise"),
        'dual'    => t("Both user's and average vote"),
      ),
    );
    $settings_form['filevoting']['text'] = array(
      '#type' => 'select',
      '#title' => t('Text to display under the stars'),
      '#default_value' => $settings['filevoting']['text'],
      '#options' => array(
        'none'    => t('No text'),
        'average' => t('Average vote'),
        'user'    => t("User's vote"),
        'smart'   => t("User's vote if available, average otherwise"),
        'dual'    => t("Both user's and average vote"),
      ),
    );
  }
}

/**
 * Implements hook_field_formatter_settings_summary_alter().
 */
function filevoting_field_formatter_settings_summary_alter(&$summary, $context) {
  $display = $context['instance']['display'][$context['view_mode']];
  $settings = $display['settings'];

  if (in_array($display['type'], _filevoting_formatters_list())) {
    $summary .= empty($summary) ? '' : '<br />';
    $summary .= (isset($settings['filevoting_enabled']) && $settings['filevoting_enabled']) ? t('Voting enabled') : t('Voting disabled');
  }
}

/**
 * Implements hook_field_attach_view_alter().
 */
function filevoting_field_attach_view_alter(&$output, $context) {
  foreach (element_children($output) as $field_name) {
    if (!in_array($output[$field_name]['#formatter'], _filevoting_formatters_list())) {
      continue;
    }

    $element = &$output[$field_name];
    $instance = field_info_instance($element['#entity_type'], $field_name, $element['#bundle']);
    $display = isset($instance['display'][$context['view_mode']]) ? $instance['display'][$context['view_mode']] : $instance['display']['default'];
    $settings = $display['settings'];

    if (!isset($settings['filevoting_enabled']) || !$settings['filevoting_enabled']) {
      continue;
    }

    foreach ($element['#items'] as $delta => $item) {
      $element[$delta]['#filevoting'] = $settings['filevoting'];
      $element[$delta]['#post_render'][] = '_filevoting_post_render';
    }
  }
}

function _filevoting_post_render($output, $element) {
  if (isset($element['#item'])) {
    $fid = $element['#item']['fid'];
  }
  elseif (isset($element['#file'])) {
    $fid = $element['#file']->fid;
  }
  if (empty($fid)) {
    return $output;
  }

  $votes = fivestar_get_votes('file', $fid, $element['#filevoting']['tag']);
  $values = array(
    'user' => $votes['user'] ? $votes['user']['value'] : 0,
    'average' => $votes['average'] ? $votes['average']['value'] : 0,
    'count' => $votes['count'] ? $votes['count']['value'] : 0,
  );

  $widgets = module_invoke_all('fivestar_widgets');
  $settings = array(
    'module' => 'filevoting',
    'stars' => $element['#filevoting']['stars'],
    'allow_clear' => $element['#filevoting']['allow_clear'],
    'allow_revote' => $element['#filevoting']['allow_revote'],
    'allow_ownvote' => $element['#filevoting']['allow_ownvote'],
    'style' => $element['#filevoting']['style'],
    'text' => $element['#filevoting']['text'],
    'microdata' => array(),

    'entity_type' => 'file',
    'entity_id' => $fid,

    'content_type' => 'file',
    'content_id' => $fid,
    'tag' => $element['#filevoting']['tag'],
    'autosubmit' => TRUE,
    'title' => FALSE,
    'labels_enable' => FALSE,
    'labels' => array(),
    'widget' => array(
      'name' => isset($widgets[$element['#filevoting']['widget']['fivestar_widget']]) ? strtolower($widgets[$element['#filevoting']['widget']['fivestar_widget']]) : 'default',
      'css' => $element['#filevoting']['widget']['fivestar_widget'],
    ),
    'description' => '',
  );
  $widget = drupal_get_form('fivestar_custom_widget', $values, $settings);
  $output .= '<div class="filevoting">' . drupal_render($widget) . '</div>';
  return $output;
}

/**
 * Implements hook_element_info_alter().
 */
function filevoting_element_info_alter(&$type) {
  // Add process handler to change the AJAX callback.
  $type['fivestar']['#process'][] = 'filevoting_expand';
}

function filevoting_expand($element) {
  if (isset($element['#settings']['module']) && $element['#settings']['module'] == 'filevoting'
    && $element['vote']['#type'] == 'select') {
     $element['vote']['#ajax']['callback'] = 'filevoting_ajax_submit';
  }

  return $element;
}

/**
 * AJAX submit handler for fivestar_custom_widget form.
 * Copy of fivestar_ajax_submit() with modifications.
 */
function filevoting_ajax_submit($form, $form_state) {
  $votes = _fivestar_cast_vote($form_state['settings']['content_type'], $form_state['settings']['content_id'], $form_state['values']['vote'], $form_state['settings']['tag']);

  $values = array();
  $values['user'] = isset($votes['user']['value']) ? $votes['user']['value'] : 0;
  $values['average'] = isset($votes['average']['value']) ? $votes['average']['value'] : 0;
  $values['count'] = isset($votes['count']['value']) ? $votes['count']['value'] : 0;

  // We need to process the 'fivestar' element with the new values.
  $form['vote']['#values'] = $values;
  $new_element = fivestar_expand($form['vote']);
  // Update the description. Since it doesn't account of our cast vote above.
  // TODO: Look into all this, I honestly don't like it and it's a bit weird.
  $form['vote']['vote']['#description'] = isset($new_element['vote']['#description']) ? $new_element['vote']['#description'] : '';

  return array(
    '#type' => 'ajax',
    '#commands' => array(
      array(
        'command' => 'fivestarUpdate',
        'data' => drupal_render($form['vote']),
      ),
    ),
  );
}

/**
 * Implements hook_fivestar_access().
 */
function filevoting_fivestar_access($entity_type, $id, $tag, $uid) {
  if ($entity_type == 'file' && file_load($id)) {
    return TRUE;
  }

  return NULL;
}

function _filevoting_formatters_list() {
  return module_invoke_all('filevoting_formatter_info');
}

/**
 * Implements hook_filevoting_formatter_info().
 */
function filevoting_filevoting_formatter_info() {
  return array(
    'image',
    'file_default',
    'file_url_plain',
    // Lightboxes
    'colorbox',
    'fancybox',
    'shadowbox', // 7.x-3.x
    'file_shadowbox', // 7.x-4.x
    'image_shadowbox', // 7.x-4.x
    'photobox',
  );
}
